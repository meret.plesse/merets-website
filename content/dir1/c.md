---
title: "Westsibirisches Tiefland"
date: 2022-01-18T11:53:48+01:00
draft: true
color: black
color2: black
---
## Wo in Russland liegt das Westsibirische Tiefland?

![alt text](https://upload.wikimedia.org/wikipedia/commons/b/b2/Sibirien_topo2.png)
[Quelle](https://de.wikipedia.org/wiki/Westsibirisches_Tiefland#/media/Datei:Sibirien_topo2.png)

Das Westsibirische Tiefland ist eine der acht Russischen Großlandschaften und liegt zwischen den Uralgebirge im Westen und dem [Mittelsibirischen Bergland](http://localhost:1313/dir1/h/)

---

>## Wie groß ist das Westsibirische Tiefland?

Die Fläche des Westsibirische Tieflands beträgt etwa 2.600.000 km².

---

>## Was charakterisiert die Landschaft?

---

Im Süden ist dei Landschaft geprägt von Sümpfen und im Norden von der Taiga, der Waldtundra und der Tundra. Im Norden wachsen durch den [Permafrost](https://www.spektrum.de/lexikon/geographie/permafrost/5921) nur noch Moose, Sträucher und Farne.

---

>## Welche Flüsse fließen durch das Westsibirische Tiefland?

Die größten Flüsse sind:
1. Irtysch
2. Jenissei
3. Ob

---
title: "Nordsibirisches Tiefland"
date: 2022-01-18T11:53:48+01:00
draft: true
color: black
color2: black
---
>## Wo liegt das Nordsibirische Tiefland?
![alt text](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/Sibirien_topo2.png/1024px-Sibirien_topo2.png)
[Quelle:Bild](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/Sibirien_topo2.png/1024px-Sibirien_topo2.png)

Das Nordsibirische Tiefland ist eine der acht Großlandschaften Russlands. Es liegt im Norden der Region Krasnojarsk.

---

>## Wie groß ist das Nordsibirische Tiefland?
Das Nordsibirische Tiefland ist in West-Ost-Richtung ca. 1400km lang und in Nord-Süd-Richtung ca. 600km.

---

>## Was charakterisiert die Landschaft?
Charakterisiert ist die Landschaft durch die Tundra und die Waldtundra. Durch den [Permafrost](https://www.spektrum.de/lexikon/geographie/permafrost/5921) im Boden wachsen hier nur Moose, Sträucher und Farne. Weiter im Süden gibt es vereinzelt Lärchenwälder.

---

>## Welche Flüsse fließen durch Nordsibirien?
Durch Nordsibirien fließen
1. der Pjassina
2. der Cheta
3. der Kotui
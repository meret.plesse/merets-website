---
title: "Mittelsibirisches Bergland"
date: 2022-01-18T11:53:48+01:00
draft: true
color: black
color2: black
---
>## Wo liegt das Mittelsibirische Bergland?
![alt text](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/Sibirien_topo2.png/1024px-Sibirien_topo2.png)
[Quelle:Bild](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/Sibirien_topo2.png/1024px-Sibirien_topo2.png)

Das Mittelsibirische Bergland ist eine der acht Russischen Großlandschaften. Es liegt im Zentrum Sibiriens zwischen den Strömen Jenissei und Lena.

---

>## Wie groß ist das Mittelsibirische Bergland?
Das Mittelsibirische Bergland ist sehr weitläufig, mit einer Fläche von 2.000.000 km². Im Norden grenzt es an das [Nordsibirische Tiefland](http://localhost:1313/dir1/g/) und im Süden an das [Südsibirische Gebirge](http://localhost:1313/dir1/i/). Im westen grenzt es an das [Westsibirische Tiefland](http://localhost:1313/dir1/c/) und im Osten an die [Mitteljakutische Niederung](http://localhost:1313/dir1/j/).

---


>## Wodurch wird die Landschaft charakterisiert?
Die Plateaus und Bergketten prägen die Landschaft. Es wachsen Nadelwälder, doch auch in dieser Großlandschaft sorgt im Norden der [Permafrost](https://www.spektrum.de/lexikon/geographie/permafrost/5921) dafür, das hier lediglich Moose und Sträucher wachsen.

---

>## Bergwelt
In der Bergwelt des Mittelsibirischen Berglandes liegen verschiedene Teilgebirge. Das[Puorana-gebirge](https://de.wikipedia.org/wiki/Putorana-Gebirge) liegt im Nordwesten, der [Jenisseirücken](https://de.wikipedia.org/wiki/Jenisseir%C3%BCcken) liegt im Südwesten, das [Wiljuplateau](https://de.wikipedia.org/wiki/Wiljuiplateau) im zentrum und das [Anabarplateau](https://de.wikipedia.org/wiki/Anabarplateau) im Norden. 

---

>## Welche Flüsse fließen durch das Mittelsibirische bergland?
1. Anabar
2. Cheta
3. jenissei
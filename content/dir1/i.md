---
title: "Südsibirische Gebirge"
date: 2022-01-18T11:53:48+01:00
draft: true
color: black
color2: black
---
>## Wo liegt das Südsibirische Gebirge?
![alt text)](https://upload.wikimedia.org/wikipedia/commons/7/70/Kuznetsk_Alatau_3.jpg)
[Quelle:Bild](https://upload.wikimedia.org/wikipedia/commons/7/70/Kuznetsk_Alatau_3.jpg)

Das Südsibirische Gebirge liegt nördlich der chinesischen und mongolischen Gebirge und südlich der [Westsibirischen Tiefenebene](http://localhost:1313/dir1/c/). 

---

>## Wie groß ist das Südsibirische Gebirge?
Das Südsibirische Gebirge ist in West-Ost-Richtung 3000 km lang.

---

>## Wovon ist die Landschaft geprägt?
Im Südsibirische Gebirge gibt es verschiedene Gebirge und Hochländer. Z.B. das Altai im Westen, mit einer Maximalhöhe von 4506m oder das Stanowoigebirge im Osten mit einer Maximalhöhe von 2412m. 

---

>## Welche Flüsse entspringen im Südsibirischen Gebirge?
im Südsibirischen Gebirge entspringen vier Flüsse:
1. Irtysch
2. Ob
3. Jenissei
4. Lena

Ein sehr bekannter See liegt im Südsibirischen Gebirge. Der Baikalsee.
{{< youtube Q32TkfdHpnI >}}
---
title: "Mitteljakutische Niederung"
date: 2022-01-18T11:53:48+01:00
draft: true
color: black
color2: black
---
>## Wo liegt die Mitteljakutische Niederung?
![alt text](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/Sibirien_topo2.png/1024px-Sibirien_topo2.png)
[Quelle:Bild](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/Sibirien_topo2.png/1024px-Sibirien_topo2.png)

Die Mittelsibirische Niederung ist eine der acht Großlandschaften Russlands. Sie liegt am Übergang von Mittel- zu Ostsibirien im Flussgebietder Lena. Sie umfasst die Unterlaufstäler von Lena und Wiljui. Im Westen grenzt sie an das [Mittelsibirische Bergland](http://localhost:1313/dir1/h/) und im Osten an das [Ostsibirische Bergland](http://localhost:1313/dir1/k/).

---

>## Wie groß ist die Mitteljakutische Niederung?
Die Mitteljakutische Niederung umfasst ca. 1.000.000 km². 

---

>## Wodurch ist die Landschaft der Mitteljakutischen Niederung geprägt?
Die Landschaft ist von Sümpfen, Nadelwald und der [Tundra](https://studyflix.de/biologie/tundra-2855) geprägt. Das weitläufige Tiefland in dem der Wiljui in die Lena mündet verengt sich Richtung Norden immer mehr zu einem Durchbruchstal. Wenn dieses passiert ist fließt der Strom in sein großes Mündungsdelta und anschließend in den Laptewsee.




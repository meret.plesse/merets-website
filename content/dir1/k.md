---
title: "Ostsibirisches Bergland"
date: 2022-01-18T11:53:48+01:00
draft: true
color: black
color2: black
---
>## Wo liegt das Ostsibirische Bergland?
![alt taxt](https://upload.wikimedia.org/wikipedia/commons/b/b2/Sibirien_topo2.png)
[Quelle:Bild](https://upload.wikimedia.org/wikipedia/commons/b/b2/Sibirien_topo2.png)

das Ostsibirische Bergland ist eine der acht Russischen Großlandschaften. Es ist ein Teil der Großregion Russisch-Fernost im Nordosten Asiens. Es liegt auf dem weitläufigen Nordost-/ Ostteil des asiatischen Kontinents.

---

>## Wodurch ist die Landschaft charakterisiert?
Im Ostsibirischen Bergland gibt es mehrere Hochgebirge, die sich über 2.700km erstrecken. In dieser Berglandschaft entspringen Flüsse wie die Jana, die Indigirka, die Kolyma, der Anadyr und der Omolon. Außerdem ist die Ostsibirische Berglandschaft nur sehr wenig besiedelt.

---

>## Welche gebirge gibt es in der Ostsibirischen Berglandschaft?
In der Ostsibirischen Berglandschaft gibt es z.B. das Werchojansker Gebirge im Westen, das Kulargebirge und das Hochland von Tschukotka im Osten.
---
title: "Ostsibirisches Tiefland"
date: 2022-01-18T11:53:48+01:00
draft: true
color: black
color2: black
---
>## Wo liegt das Ostsibirische Tiefland?
![alt text](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/Sibirien_topo2.png/1024px-Sibirien_topo2.png)
[Quelle:Bild](https://upload.wikimedia.org/wikipedia/commons/thumb/b/b2/Sibirien_topo2.png/1024px-Sibirien_topo2.png)

Das Ostsibirische Tiefland ist eine der acht Russischen Großlandschaften. Es befindet sich südlich des Ostsibirischen Sees und grenzt im Westen, Süden und Osten an das Ostsibirische Bergland. 

* * *

>## Wodurch ist das Landschaftsbild geprägt?
Das Landschaftsbild des Ostsibirischen Tieflandes ist stark geprägt von der Tundra. Die Flüsse sorgen im Winter dafür. dass die Landschaft verformt wird.

---

>## Welche Flüsse fließen durch das Ostsibirische Tiefland?
Durch das Ostsibirische Tiefland fließen:
1. die Alaseja
2. die jana 
3. die Indigirka
4. die Kolyma  


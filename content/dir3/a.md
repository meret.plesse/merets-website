---
title: "Der kälteste Ort der Welt"
date: 2022-01-18T09:42:02+01:00
draft: true
color: darkblue
---


{{< youtube NRyfLGa4YqE >}}

Russland liegt inmitten der kältesten Klimazonen der Erde. Die kälteste Stadt der Welt heißt Jakutsk und liegt im tiefsten Sibirien. Der Winter dauert in dieser Stadt acht Monate und die Temperaturen fallen auf bis zu -60 Grad Celsius. der Rekord liegt bei -71,5 Grad.


